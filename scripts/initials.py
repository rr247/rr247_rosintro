# -*- coding: utf-8 -*-

from __future__ import print_function

import sys
import rospy
import moveit_commander

class initialsUR(object):

    def __init__(self):
        super(initialsUR, self).__init__()
        
        # initialize 'moveit_commander' and a 'rospy' node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("initials", anonymous=True)

        # instantiate a 'MoveGroupCommander' object to move robot
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        # misc variables
        self.move_group = move_group

    def go_to_R(self): #draw 'R'
        #base
        self.go_to_joint_states([-0.0524, 0.209, -1.87, -1.48, 0.0524, -3.14])
        #top left
        self.go_to_joint_states([-0.0698, -0.454, -1.01, -1.68, 0.0698, -3.14])
        #top right
        self.go_to_joint_states([-0.122, -0.506, -1.38, -1.26, 0.122, -3.14])
        #bottom right of square
        self.go_to_joint_states([-0.122, -0.0524, -1.97, -1.12, 0.122, -3.14])
        #bottom left of square
        self.go_to_joint_states([-0.0873, 0.0524, -1.66, -1.52, 0.0873, -3.14])
        #bottom right
        self.go_to_joint_states([-0.14, 0.0698, -2.25, -0.873, 0.175, -3.14])
        
        return
    
    def go_to_I(self): #draw 'I'
        #base
        self.go_to_joint_states([-0.0524, 0.209, -1.87, -1.48, 0.0524, -3.14])
        #vertical line for I
        self.go_to_joint_states([-0.0524, -0.262, -1.38, -1.5, 0.0524, -3.14])
        
        return
    
    def go_to_S(self): #draw 'S'
        #base
        self.go_to_joint_states([-0.0524, 0.209, -1.87, -1.48, 0.0524, -3.14])
        #bottom right
        self.go_to_joint_states([-0.14, 0.0698, -2.25, -0.873, 0.175, -3.14])
        #middle right
        self.go_to_joint_states([-0.0873, -0.157, -1.97, -1.01, 0.0873, -3.14])
        #middle left
        self.go_to_joint_states([-0.0524, -0.0175, -1.62, -1.5, 0.0524, -3.14])
        #top left
        self.go_to_joint_states([-0.0524, -0.244, -1.36, -1.54, 0.0524, -3.14])
        #top right
        self.go_to_joint_states([-0.0873, -0.349, -1.68, -1.12, 0.0873, -3.14])
        
        return
    
    def go_to_joint_states(self, joint_vals):
        # given a robot and an array of joint values, rotate robot's joints to 
        # the joint values specified in the array
        move_group = self.move_group
        
        # get joint values and update them
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = joint_vals[0]
        joint_goal[1] = joint_vals[1]
        joint_goal[2] = joint_vals[2]
        joint_goal[3] = joint_vals[3]
        joint_goal[4] = joint_vals[4]
        joint_goal[5] = joint_vals[5]
        
        # The go command is called with joint values
        move_group.go(joint_goal, wait=True)
        
        # Calling stop() ensures that there is no residual movement
        move_group.stop()
        
        return
      
def main():
    
    # instantiate a bot    
    bot = initialsUR()
    
    # draw 'R', then 'I', then 'S'
    bot.go_to_R()
    bot.go_to_I()
    bot.go_to_S()

if __name__ == "__main__":
    main()