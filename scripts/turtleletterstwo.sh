#!/usr/bin/bash

rosservice call /reset

rosservice call /turtle1/set_pen '255' '0' '0' '5' '0'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, 3.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[1.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[2.5, 0.0, 0.0]' '[0.0, 0.0, -3.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[1.0, 0.0,0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, 0.0, 0.0]' '[0.0, 0.0, 2.3]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[2.1, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rosservice call /turtle1/set_pen '0' '255' '0' '5' '255'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0, 0, 0]' '[0, 0, 0.7]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[1, 0, 0]' '[0, 0, 0]'

rosservice call /turtle1/set_pen '0' '255' '0' '5' '0'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[2, 0, 0]' '[0, 0, 0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[-1, 0, 0]' '[0, 0, 0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0, 3, 0]' '[0, 0, 0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[-1, 0, 0]' '[0, 0, 0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[2, 0, 0]' '[0, 0, 0]'
