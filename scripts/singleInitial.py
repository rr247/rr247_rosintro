# -*- coding: utf-8 -*-
"""
Created on Sun Oct  9 10:50:07 2022

@author: 16092
"""
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

class singleInitialUR(object):
    """SingleInitialUR"""

    def __init__(self):
        super(singleInitialUR, self).__init__()
        
        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("single_initial", anonymous=True)

        ## Instantiate a `RobotCommander`_ object. 
        robot = moveit_commander.RobotCommander()

        ## Instantiate a `PlanningSceneInterface`_ object.  
        scene = moveit_commander.PlanningSceneInterface()

        ## Instantiate a `MoveGroupCommander`_ object.
        ## This interface can be used to plan and execute motions:
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        ## Create a `DisplayTrajectory` ROS publisher which is used to display
        ## trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        # Misc variables
        self.move_group = move_group

    def go_to_R(self):
        #base
        self.go_to_joint_states([-0.0524, 0.209, -1.87, -1.48, 0.0524, -3.14])
        #top left
        self.go_to_joint_states([-0.0698, -0.454, -1.01, -1.68, 0.0698, -3.14])
        #top right
        self.go_to_joint_states([-0.122, -0.506, -1.38, -1.26, 0.122, -3.14])
        #bottom right of square
        self.go_to_joint_states([-0.122, -0.0524, -1.97, -1.12, 0.122, -3.14])
        #bottom left of square
        self.go_to_joint_states([-0.0873, 0.0524, -1.66, -1.52, 0.0873, -3.14])
        #bottom right
        self.go_to_joint_states([-0.14, 0.0698, -2.25, -0.873, 0.175, -3.14])
        
        return
        
    def go_to_joint_states(self, joint_vals):
        move_group = self.move_group
        
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = joint_vals[0]
        joint_goal[1] = joint_vals[1]
        joint_goal[2] = joint_vals[2]
        joint_goal[3] = joint_vals[3]
        joint_goal[4] = joint_vals[4]
        joint_goal[5] = joint_vals[5]
        
        # The go command is called with joint values
        move_group.go(joint_goal, wait=True)
        
        # Calling stop() ensures that there is no residual movement
        move_group.stop()
        
        return
      
def main():
    
        bot = singleInitialUR()

        bot.go_to_R()

if __name__ == "__main__":
    main()
